<?php
/**
 * Template Name: Actividades - Desktop Slides
 *
 */

get_header(); 

?>
	


<?php if( wp_is_mobile() ) : ?>

  <?php get_template_part('parts/mobile-actividades'); ?>
<?php else : ?>

   <?php get_template_part('parts/desktop-activities-slides'); ?>
<?php endif; ?>



		
<?php get_footer(); ?>

<?php if( wp_is_mobile() ) : ?>
  <script>
  	/* activate the swiper slider in activities section */
    var swiper = new Swiper('.swiper-container', {
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      slidesPerView: 1,
      paginationClickable: true,
      spaceBetween: 30,
      //loop: true
    });
  </script>
<?php else: ?>
  <script>
    var swiper = new Swiper('.swiper-container', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 5,
        freeMode: true
    });
  </script>
<?php endif; ?>