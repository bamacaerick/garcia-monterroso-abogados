<?php
/**
 * Template Name: Contactenos
 *
 */

get_header(); 

$customFields = get_post_custom();

// echo '<pre>';
// print_r($customFields);
// echo '</pre>';

?>
	
<div class="contact container clear">
		
  <div class="contact__form">
    <div class="contact__social">
      
      <?php if( isset($customFields['wpcf-facebook_url'][0]) && !empty($customFields['wpcf-facebook_url'][0]) ) : ?>
        <a href="<?php echo $customFields['wpcf-facebook_url'][0]; ?>" title="Síguenos en Facebook" class="fb-icon">
          <span>Síguenos en Facebook</span>
        </a>
      <?php endif; ?>
      <?php if( isset($customFields['wpcf-twitter_url'][0]) && !empty($customFields['wpcf-twitter_url'][0]) ) : ?>
        <a href="<?php echo $customFields['wpcf-twitter_url'][0]; ?>" title="Síguenos en Twitter" class="tw-icon">
          <span>Síguenos en Twitter</span>
        </a>
      <?php endif; ?>
      <?php if( isset($customFields['wpcf-blog_url'][0]) && !empty($customFields['wpcf-blog_url'][0]) ) : ?>
        <a href="<?php echo $customFields['wpcf-blog_url'][0]; ?>" title="Visita nuestro Blog" class="bl-icon">
          <span>Visita nuestro Blog</span>
        </a>
      <?php endif; ?>
    </div>
    <?php echo do_shortcode('[contact-form-7 id="23" title="Contact form 1"]'); ?>
  </div>
  <div class="contact__map">
    <?php if( isset($customFields['wpcf-map_embed'][0]) && !empty($customFields['wpcf-map_embed'][0]) ) {
        echo $customFields['wpcf-map_embed'][0];
      }?>
  </div>
	
</div>


		
<?php get_footer(); ?>
