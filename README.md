# README #

Wordpress custom theme for García Monterroso Abogados based on design delivered by Pablo Dávila.
This theme has been developed using custom templates for some requirements and sass preprocessor 
for CSS stylesheet based on BEM class name methodology and smacss concepts. 

All sass configuration is using gulp as task runner.

### How do I get set up? ###
* As a regular theme installation and activation on wordpress CMS
* Database configuration is a MySQL


### How do I get set up styles section? ###
* Make sure you have installed node.js and gulp package (If not then install them)
* Run `npm install` on the sass folder where you can see the gulpfile
* Once you run the above command it will download all dependencies which will process all styles from sass files


### Who do I talk to? ###

* Repo owner or admin
* bamacaerick@gmail.com