<?php
/**
 * Template Name: Inicio
 *
 */

get_header(); 

$customFields = get_post_custom();

?>
	
	<div class="home">
		<div class="home__content">
			
			<div class="home__content-data">
				<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

					if( isset( $customFields['wpcf-video_field'][0] ) && !empty( $customFields['wpcf-video_field'][0] ) ) {
						echo '<iframe src="https://www.youtube.com/embed/'. $customFields['wpcf-video_field'][0] .'" frameborder="0" allowfullscreen></iframe>';

					}elseif ( isset( $customFields['wpcf-image_field'][0] ) && !empty( $customFields['wpcf-image_field'][0] ) ) {
						echo '<img src="'. $customFields['wpcf-image_field'][0] .'" />';
					}
					// End the loop.
					endwhile;
				?>
			</div>

			<div class="home__content-logo">
				<?php

		  		if ( is_front_page() || is_home() ) : ?>
		  			<h1 class="site-title">
		  				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>">
		            <span>
		              <?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>
		            </span>
		  				</a>
		  			</h1>
		  		<?php else : ?>
		  			<p class="site-title">
		          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>">
		            <span>
		              <?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>
		            </span>   
		          </a>
		        </p>
		  		<?php endif; ?>
			</div>
		</div>
	</div>
		
<?php get_footer(); ?>
