var $ = jQuery;
jQuery(document).ready(function(){

	
   // do jQuery //
   

   /* call mobile rules */
   if( $('#mobile-content').length > 0 ){
      mobileRules();
   }else{
      desktopRules();
   }

})

function mobileRules() {
   

   /* activate mobile menu */

   $('.menu__icon-mobile').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();

      $(this).toggleClass('open_menu');

      $('.menu__nav').slideToggle('fast');
   });

   // clients section
   clientsSlider();

   activitiesSlider();
}

function desktopRules() {

   // activities section 
   $('.actividades-content__item').first().addClass('current');
   $('.actividades-sidebar__item').first().addClass('current');

   $('.actividades-sidebar__item').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();

      var postId = $(this).data('id');

      $('.actividades-content__item, .actividades-sidebar__item').removeClass('current');
      $('#post-'+postId).addClass('current');
      $(this).addClass('current');

   });

   //clients section
   clientsSlider();

   activitiesSlider();
}

function clientsSlider(){
   /* clients section animations */
   $('.clients__item').first().addClass('current_item');

   $('.clientes__logo-item img').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();

      var showPost = $(this).data('id');

      $('.clients__item').removeClass('current_item');
      $('#'+showPost).addClass('current_item');
   });
}

function activitiesSlider(){
   /* clients section animations */
   $('.activities-s__item').first().addClass('current_item');

   $('.activities-slide__item span').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();

      var showPost = $(this).data('id');

      $('.activities-s__item').removeClass('current_item');
      $('#'+showPost).addClass('current_item');
   });
}