<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
  <title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>"></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

  <?php get_template_part('parts/header-social-tags'); ?>

	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
</head>

<body <?php body_class(); ?>>

  <div class="main-content" <?php echo (wp_is_mobile() ? 'id="mobile-content"' : 'id="desktop-content"'); ?>>
  	
    <div class="header">
  		<?php

  		if ( is_front_page() || is_home() ) : ?>
  			<h1 class="site-title">
  				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>">
            <span>
              <?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>
            </span>
  				</a>
  			</h1>
  		<?php else : ?>
  			<p class="site-title">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>">
            <span>
              <?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description', 'display' ); ?>
            </span>   
          </a>
        </p>
        <?php if( wp_is_mobile() ) : ?>
          <h1 class="section-title"><?php the_title(); ?></h1>
        <?php endif; ?>
  		<?php endif; ?>
  	</div>
    <!-- closing header -->

  	<div class="menu">
      <span class="menu__icon-mobile"></span>
  		<?php wp_nav_menu(array('menu_class'     => 'menu__nav',) ); ?>
  	</div>
    <!-- closing menu -->

