<?php
/**
 * Template Name: Actividades
 *
 */

get_header(); 

?>
	
  <!-- Call mobile or destkop loop for activities -->
  <?php if( wp_is_mobile() ) : ?>

    <?php get_template_part('parts/mobile-actividades'); ?>
  <?php else : ?>

     <?php get_template_part('parts/desktop-actividades'); ?>
  <?php endif; ?>
	

<?php get_footer(); ?>

<?php if( wp_is_mobile() ) : ?>
  
<script>
	/* activate the swiper slider in activities section */
  var swiper = new Swiper('.swiper-container', {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 1,
    paginationClickable: true,
    spaceBetween: 30,
    //loop: true
  });
</script>
<?php endif; ?>