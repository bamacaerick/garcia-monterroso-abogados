<?php 
/* ---------- the systemscripts base theme ----------------------*/


/* ----------- // ENQUEUE SCRIPTS // ----------- */
/* ----------- // ====== ====== // ----------- */

function wpdocs_gmabogados_scripts() {
    wp_enqueue_style( 'gmabogados-styles', get_stylesheet_uri(), '', null, 'all' );
    wp_enqueue_script( 'gmabogados-js', get_template_directory_uri() . '/ui/js/general.js', array(), '1.0.0', true );
    if ( is_page_template( 'page-actividades.php' ) || is_page_template( 'page-clientes.php' ) || is_page_template( 'page-hybrid-actividades.php' ) ) {
    	wp_enqueue_style( 'swiper-styles', get_template_directory_uri() . '/ui/css/swiper.css', array(), null, 'all'  );
    	wp_enqueue_script( 'swiper', get_template_directory_uri() . '/ui/js/swiper.jquery.js', array(), '1.0.0', true );
  	}
}
add_action( 'wp_enqueue_scripts', 'wpdocs_gmabogados_scripts' );



/* ----------- // CUSTOM PANELS // ----------- */
/* ----------- // ====== ====== // ----------- */

add_action('init', 'gamoPostTypes');

function gamoPostTypes() {
	
	/*=== Actividades ===*/
	$actividadesLabels = array(
	    'name' => _x('Actividades', 'post type general name'),
	    'singular_name' => _x('Actividades', 'post type singular name'),
	    'add_new' => _x('Añadir Nueva', 'Actividad'),
	    'add_new_item' => __('Añadir Nueva Actividad'),
	    'edit_item' => __('Editar Actividad'),
	    'new_item' => __('Añadir Actividad'),
	    'view_item' => __('Ver Actividades'),
	    'search_items' => __('Buscar Actividades'),
	    'not_found' =>  __('Actividad no existente.'),
	    'not_found_in_trash' => __('No se encontraron Actividades en la papelera.'), 
	    'parent_item_colon' => '',
	    'menu_name' => 'Actividades'
	);
	
	register_post_type(
	    'actividades', 
	    array('labels' => $actividadesLabels,
		'description' => 'actividades',
		'publicly_queryable' => true,
		'public' => true, 
		'show_ui' => true,
		'hierarchical' => false, // like posts
		'supports' => array(
		    'title',
		    'editor',
		    'thumbnail')
		)
	);
	
	/*=== Clientes ===*/
	$clientesLabels = array(
	    'name' => _x('Clientes', 'post type general name'),
	    'singular_name' => _x('Clientes', 'post type singular name'),
	    'add_new' => _x('Añadir Nuevo', 'Cliente'),
	    'add_new_item' => __('Añadir Nuevo Cliente'),
	    'edit_item' => __('Editar Cliente'),
	    'new_item' => __('Nuevo Cliente'),
	    'view_item' => __('Ver Clientes'),
	    'search_items' => __('Buscar Clientes'),
	    'not_found' =>  __('No se encontraron Clientes'),
	    'not_found_in_trash' => __('No se encontraron Clientes en la papelera'), 
	    'parent_item_colon' => '',
	    'menu_name' => 'Clientes'
	);
	
	register_post_type(
	    'clientes', 
	    array('labels' => $clientesLabels,
		'description' => 'clientes',
		'publicly_queryable' => true,
		'public' => true, 
		'show_ui' => true,
		'hierarchical' => false, // like posts
		'supports' => array(
		    'title',
		    'editor',
		    'thumbnail')
		)
	);
}

function get_logo($custom_data) {
  $customFields = $custom_data;
  if( isset( $customFields['wpcf-logo_cliente_color'][0] ) && !empty( $customFields['wpcf-logo_cliente_color'][0] ) ) {
                
    $logo['color_image'] = $customFields['wpcf-logo_cliente_color'][0];
  } else {

    $logo['color_image'] = '/wp-content/uploads/default-color.png';
  }

  if( isset( $customFields['wpcf-logo_cliente_negro'][0] ) && !empty( $customFields['wpcf-logo_cliente_negro'][0] ) ) {
    
    $logo['gray_image'] = $customFields['wpcf-logo_cliente_negro'][0];
  } else {
    
    $logo['gray_image'] = '/wp-content/uploads/default-negro.png';
  }

  return $logo;
}