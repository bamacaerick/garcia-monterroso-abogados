<div class="activities container clear">
  <div class="swiper-container">

    <?php  $activities = new WP_Query('post_type=actividades&order=DESC'); ?>

    <?php if ( $activities->have_posts()) :?>

      <ul class="activities__list swiper-wrapper">
        
    

      <?php while ($activities->have_posts() ) : $activities->the_post(); ?>

        <li class="activities__item swiper-slide">
          
          <div class="activities__content">
            <div class="activities__header">
              <h2 class="activities__title"><?php the_title(); ?></h2>
            </div>
            <div class="activities__body wp_editor_content">
              <?php the_content(); ?>
            </div>    
          </div>
        
        </li>

      <?php endwhile; ?>
      
    <?php endif; ?>
    
    </ul>
    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    
  </div>
</div>