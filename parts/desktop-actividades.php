<?php  $activities = new WP_Query('post_type=actividades&order=DESC'); ?>

<?php if ( $activities->have_posts()) :?>
  
  <ul class="actividades-sidebar">
    <?php while ($activities->have_posts() ) : $activities->the_post(); ?>

      <li class="actividades-sidebar__item" data-id="<?php the_ID(); ?>" title="<?php the_title(); ?>">
        <span><?php the_title(); ?></span>
      </li>

    <?php endwhile; ?>
  </ul>

<?php endif; ?>

<?php if ( $activities->have_posts()) :?>
  
  <div class="actividades-content">
    <?php while ($activities->have_posts() ) : $activities->the_post(); ?>

      <div class="actividades-content__item wp_editor_content" id="post-<?php the_ID(); ?>">
        <?php the_content(); ?>
      </div>

    <?php endwhile; ?>
  </div>

<?php endif; ?>


