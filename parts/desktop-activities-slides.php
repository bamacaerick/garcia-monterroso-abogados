<div class="activities-s container">
    
  <div class="activities-s__container">

    <!-- Calling logos part -->
    <?php 

      get_template_part('parts/actividades-slide');
    ?>


    <!-- Displaying activities-s content -->
    <?php  $activities = new WP_Query('post_type=actividades&order=DESC'); ?>
      
      <?php if ( $activities->have_posts()) :?>
      
      <ul class="activities-s__list">
      
        <?php while ($activities->have_posts() ) : $activities->the_post(); ?>

          <li id="post-<?php the_ID(); ?>" class="activities-s__item">
            <div class="activities-s__color-logo">
              <h2 class="activities-s__title"><?php the_title(); ?></h2>
            </div>
            <div class="wp_editor_content"> 
              <?php the_content(); ?>
            </div>
          </li>   
        <?php endwhile; ?>
      
      </ul>

    <?php endif; ?>

  </div>
</div>