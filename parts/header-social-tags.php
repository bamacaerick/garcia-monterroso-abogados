  <!-- FB meta tags -->
    <meta property="og:title" content="García Monterroso Abogados" />
    <meta property="og:description" content=" Consultoría legal, laboral, inmobiliario, migratorio, mercantil, civil, notariado"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://garciamonterrosoabogados.com" />
    <meta property="og:image" content="http://garciamonterrosoabogados.com/wp-content/uploads/2017/07/Para-web_PAra-web.jpg" />
    <meta property="og:image" content="http://garciamonterrosoabogados.com/wp-content/uploads/2017/07/Para-web_PAra-web.jpg" />
    <meta property="og:image" content="http://garciamonterrosoabogados.com/wp-content/uploads/2017/07/Para-web_PAra-web.jpg" />
    <meta property="og:image" content="http://garciamonterrosoabogados.com/wp-content/uploads/2017/07/Para-web_PAra-web.jpg" />

  <!-- Twitter meta tags -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="http://garciamonterrosoabogados.com" />
    <meta name="twitter:title" content="García Monterroso Abogados" />
    <meta name="twitter:description" content="Consultoría legal, laboral, inmobiliario, migratorio, mercantil, civil, notariado" />
    <meta name="twitter:image" content="http://garciamonterrosoabogados.com/wp-content/uploads/2017/07/Para-web_PAra-web.jpg" />