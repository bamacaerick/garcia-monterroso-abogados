<?php  

  $activities = new WP_Query('post_type=actividades&order=DESC');
 
  if ( $activities->have_posts()) {

    echo '<div class="activities-slide__container swiper-container">';
      echo '<div class="activities-slide__wrapper swiper-wrapper">';

        while ($activities->have_posts() ){
          $activities->the_post();

          $logos = get_logo(get_post_custom());

          // displaying all logos
          echo '<div class="activities-slide__item swiper-slide">';
            echo '<span data-id="post-'. get_the_ID() .'">' . get_the_title() .'</span>';
          echo '</div>';
        } 

      echo '</div>';
      // clossing activities-slide__logos
      // 
   
    echo '</div>';
    // clossing logo__wrapper
    
    echo '<div class="swiper-button-prev"></div>';
    echo '<div class="swiper-button-next"></div>';
      

  }
  wp_reset_query();
?>