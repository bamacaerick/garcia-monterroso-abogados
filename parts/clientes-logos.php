<?php  

  $clients = new WP_Query('post_type=clientes&order=DESC');
 
  if ( $clients->have_posts()) {

    echo '<div class="clientes__logos-container swiper-container">';
      echo '<div class="clientes__logos-wrapper swiper-wrapper">';

        while ($clients->have_posts() ){
          $clients->the_post();

          $logos = get_logo(get_post_custom());

          // displaying all logos
          echo '<div class="clientes__logo-item swiper-slide">';
            echo '<img src="'. $logos['gray_image'] .'" alt="'. get_the_title() .'" data-image="'. $logos['color_image'] .'" data-id="post-'. get_the_ID() .'">';
          echo '</div>';
        } 

      echo '</div>';
      // clossing clientes__logos
      // 
   
    echo '</div>';
    // clossing logo__wrapper
    
    echo '<div class="swiper-button-prev"></div>';
    echo '<div class="swiper-button-next"></div>';
      

  }
  wp_reset_query();
?>