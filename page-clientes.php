<?php
/**
 * Template Name: Clientes
 *
 */

get_header(); 
?>
	
<div class="clients container">
		
	<div class="clients__container">

		<!-- Calling logos part -->
		<?php 

			get_template_part('parts/clientes-logos');
		?>


		<!-- Displaying clients content -->
	 	<?php  $clients = new WP_Query('post_type=clientes&order=DESC'); ?>
   	 	
   	 	<?php if ( $clients->have_posts()) :?>
			
			<ul class="clients__list">
	    
	      <?php while ($clients->have_posts() ) : $clients->the_post(); ?>

	      	<?php $logos = get_logo(get_post_custom()); ?>
				
					<li id="post-<?php the_ID(); ?>" class="clients__item">
						<div class="clients__color-logo">
							<img src="<?php echo $logos['color_image'] ?>" alt="<?php the_title(); ?>">
						</div>
						<h2 class="clients__title"><?php the_title(); ?></h2>
						<div class="wp_editor_content"> 
							<?php the_content(); ?>
						</div>
					</li>		
				<?php endwhile; ?>
			
			</ul>

		<?php endif; ?>

	</div>
</div>
		
<?php get_footer(); ?>

<?php if( wp_is_mobile() ) : ?>
	<script>
	var swiper = new Swiper('.swiper-container', {
	   	nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    slidesPerView: 3,
	    paginationClickable: true,
	    freeMode: true
	});
	</script>
<?php else: ?>
	<script>
		var swiper = new Swiper('.swiper-container', {
		   	nextButton: '.swiper-button-next',
		    prevButton: '.swiper-button-prev',
		    slidesPerView: 5,
		    paginationClickable: true,
		    freeMode: true
		});
	</script>
<?php endif; ?>